﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Robo.Resources;
using Microsoft.Phone.Media;
using System.Diagnostics;
using MjpegProcessor;
using Windows.Devices.Input;
using WP8_Joystick;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Threading;
using Robo.Classes;
using Microsoft.Devices.Sensors;
using System.Windows.Threading;
using Microsoft.Xna.Framework;
using Windows.UI.Input;

namespace Robo
{
    public partial class MainPage : PhoneApplicationPage
    {
        MjpegDecoder _mjpeg;
        Uri streamLocation = new Uri ("http://192.168.0.250:8080/?action=stream");
        string moveLocation = "192.168.0.200";
        int movementPort = 2000;
        SocketClient client = new SocketClient();
        int maxCoeficient = 55;
        int maxSpeed = 255;
        int minSpeed = 200;
        int minVertical = 5;
        int maxVertical = 150;
        int minHorizontal = 110;
        int maxHorizontal = 160;

        // Constructor
        public MainPage()
        {
            InitializeComponent();

            //Video player
            _mjpeg = new MjpegDecoder();
            _mjpeg.FrameReady += mjpeg_FrameReady;
            try
            {
                _mjpeg.ParseStream(streamLocation);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Camera unreachable");
            }
        }

        private void mjpeg_FrameReady(object sender, FrameReadyEventArgs e)
        {
            videoPlayer.Source = e.BitmapImage;
        }


        void joystick1_NewCoordinates(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void Joystick_NewCoordinates_1(object sender, EventArgs e)
        {
            int direction = ((MyCoordinates)e).Direction;
            int speed = ((MyCoordinates)e).Speed;
            int speedLeft = 0, speedRight = 0;


            float speedCoeficient = ((float)speed / (float)maxSpeed);

            //Sets speeds
            if((direction >= 0) && (direction <= 90))
            {
                speedLeft = minSpeed + ((int)(maxCoeficient * speedCoeficient));
                speedRight = minSpeed + ((int)(maxCoeficient * (1 - speedCoeficient)));
            }
            else if ((direction > 90) && (direction <= 180))
            {
                speedLeft = (minSpeed + ((int)(maxCoeficient * speedCoeficient))) * (-1);
                speedRight = (minSpeed + ((int)(maxCoeficient * (1 - speedCoeficient)))) * (-1);
            }
            else if ((direction > 180) && (direction <= 270))
            {
                speedLeft = (minSpeed + ((int)(maxCoeficient * (1 - speedCoeficient)))) * (-1);
                speedRight = (minSpeed + ((int)(maxCoeficient * speedCoeficient))) * (-1);
            }
            else if ((direction > 270) && (direction <= 360))
            {
                speedLeft = minSpeed + ((int)(maxCoeficient * (1 - speedCoeficient)));
                speedRight = minSpeed + ((int)(maxCoeficient * speedCoeficient));
            }

            sendMovement(speedRight.ToString(), speedLeft.ToString());
        }

        private void joystick1_Stop(object sender, EventArgs e)
        {
            sendMovement("0", "0");
        }

        private void sendMovement(string speedRight, string speedLeft)
        {
            Debug.WriteLine("Writing speedLeft={0} sideRight={1}", speedLeft, speedRight);
            string resultRight = client.Send("r" + speedRight + "\r\n");
            string resultLeft = client.Send("l" + speedLeft + "\r\n");
        }





        void joystick2_NewCoordinates(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void Joystick_NewCoordinates_2(object sender, EventArgs e)
        {
            int direction = ((MyCoordinates)e).Direction;
            int speed = ((MyCoordinates)e).Speed;
            double moveVertical = 0, moveHorizontal = 0;
            double vertical = 0, horizontal = 0;
            double centerVertical = Math.Round((float)(maxVertical) / 2);
            double centerHorizontal = Math.Round((float)(maxHorizontal) / 2);

            float speedCoeficient = ((float)speed / (float)maxSpeed);

            //Sets speeds
            if ((direction >= 0) && (direction <= 90))
            {
                moveVertical = centerVertical * speedCoeficient;
                moveHorizontal = centerHorizontal * speedCoeficient;

                vertical = centerVertical + moveVertical;
                horizontal = centerHorizontal + moveHorizontal;

                if (vertical > maxVertical) vertical = maxVertical;
                if (horizontal > maxHorizontal) horizontal = maxHorizontal;
            }
            else if ((direction > 90) && (direction <= 180))
            {
                moveVertical = centerVertical * speedCoeficient;
                moveHorizontal = centerHorizontal * speedCoeficient;

                vertical = centerVertical - moveVertical;
                horizontal = centerHorizontal + moveHorizontal;

                if (vertical < minVertical) vertical = minVertical;
                if (horizontal > maxHorizontal) horizontal = maxHorizontal;
            }
            else if ((direction > 180) && (direction <= 270))
            {
                moveVertical = centerVertical * speedCoeficient;
                moveHorizontal = centerHorizontal * speedCoeficient;

                vertical = centerVertical - moveVertical;
                horizontal = centerHorizontal - moveHorizontal;

                if (vertical < minVertical) vertical = minVertical;
                if (horizontal < minHorizontal) horizontal = minHorizontal;
            }
            else if ((direction > 270) && (direction <= 360))
            {
                moveVertical = centerVertical * speedCoeficient;
                moveHorizontal = centerHorizontal * speedCoeficient;

                vertical = centerVertical + moveVertical;
                horizontal = centerHorizontal - moveHorizontal;

                if (vertical > maxVertical) vertical = maxVertical;
                if (horizontal < minHorizontal) horizontal = minHorizontal;
            }

            sendCamera(Math.Round(horizontal).ToString(), Math.Round(vertical).ToString());
        }

        private void joystick2_Stop(object sender, EventArgs e)
        {
            Debug.WriteLine("ssds");
            double centerVertical = Math.Round((float)(maxVertical) / 2);
            double centerHorizontal = Math.Round((float)(maxHorizontal) / 2);
            sendCamera(centerVertical.ToString(), centerHorizontal.ToString());
        }

        private void sendCamera(string moveVertical, string moveHorizontal)
        {
            Debug.WriteLine("writing movehorizontal={0} movevertical={1}", moveHorizontal, moveVertical);
            string resultRight = client.Send("v" + moveVertical + "\r\n");
            string resultLeft = client.Send("h" + moveHorizontal + "\r\n");
        }





        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            client.Connect(moveLocation, movementPort);
            Debug.WriteLine("Started");

            joystick1.StartJoystick();
            joystick2.StartJoystick();
            base.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            client.Close();
            Debug.WriteLine("Ended");

            joystick1.StopJoystick();
            joystick2.StopJoystick();
            base.OnNavigatedFrom(e);
        }
    }
}